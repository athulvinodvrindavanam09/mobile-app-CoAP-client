package com.example.TeslaModelRCApp;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.Random;

public class Services extends Service {

    private static final String TAG = "Service";
    private static final String radarResource = "/getRadars";
    private static final String heartBeatResource = "/heartbeat";
    private IBinder mBinder = new MyBinder();
    private final Random mGenerator = new Random();
    private Handler mHandler;
    private Boolean mIsPaused;
    private int counter = 0;

    int leftSensor = 0;
    int middleSensor = 0;
    int rightSensor = 0;
    int rearSensor = 0;

    @Override
    public void onCreate(){
        super.onCreate();
        mHandler = new Handler();
        mIsPaused = false;
        startTask();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public class MyBinder extends Binder {
        Services getService(){
            return Services.this;
        }
    }

    public void startTask(){
        final Runnable runnable = new Runnable(){
            public void run() {
                if(MainActivity.getInstance() == null)
                {

                }
                else {
                    if(MainActivity.getInstance().wifiManager.isWifiEnabled()){
//                        if(counter % 2 == 1) {
                            MainActivity.getInstance().coapClient.sendRequest(radarResource);
//                        }
//                        if (counter % 10 == 0) {
                            MainActivity.getInstance().coapClient.sendRequest(heartBeatResource);
//                        }
                    }
                    if(!MainActivity.getInstance().wifiManager.isWifiEnabled())
                    {
                        sensorfragment.getINSTANCE().heartbeat.setText("Heartbeat Not Received...");
                    }
                    mHandler.postDelayed(this, 100);
                }
            }
        };
        mHandler.postDelayed(runnable, 100);
    }

    private void pauseTask() {
        mIsPaused = true;
    }
    private void unpauseTask(){
        mIsPaused = false;
        startTask();
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        stopSelf();
    }

    public int getRandomNumber() {
        return mGenerator.nextInt(40);
    }
}
