package com.example.TeslaModelRCApp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class sensorfragment extends Fragment {

    private static sensorfragment INSTANCE = null;

    Handler refresh = new Handler(Looper.getMainLooper());

    View view;

    TextView heartbeat = null;
    TextView leftDebug = null;
    TextView middleDebug = null;
    TextView rightDebug = null;
    TextView rearDebug = null;
    ProgressBar leftProgressBar = null;
    ProgressBar middleProgressBar= null;
    ProgressBar rightProgressBar = null;
    ProgressBar rearProgressBar = null;

    public int middle = 0;
    public int left = 0;
    public int right = 0;
    public int rear = 0;

    public String middletxt = "";
    public String lefttxt = "";
    public String righttxt = "";
    public String reartxt = "";

    @SuppressLint("ValidFragment")
    public sensorfragment(){

    }

    public ArrayList<TextView> getTextView()
    {
        ArrayList<TextView> debug = new ArrayList<TextView>();
        leftDebug = (TextView) view.findViewById(R.id.textView3);
        middleDebug = (TextView) view.findViewById(R.id.textView5);
        rightDebug = (TextView) view.findViewById(R.id.textView4);
        rearDebug = (TextView) view.findViewById(R.id.textView6);
        debug.add(leftDebug);
        return debug;
    }


    public static sensorfragment getINSTANCE(){
        if(null == INSTANCE)
            INSTANCE = new sensorfragment();
        return INSTANCE;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.sensorfragment, container, false);

        leftDebug = (TextView) view.findViewById(R.id.textView3);
        middleDebug = (TextView) view.findViewById(R.id.textView5);
        rightDebug = (TextView) view.findViewById(R.id.textView4);
        rearDebug = (TextView) view.findViewById(R.id.textView6);

        leftProgressBar = (ProgressBar) view.findViewById(R.id.leftSensor);
        middleProgressBar = (ProgressBar) view.findViewById(R.id.middleSensor);
        rightProgressBar = (ProgressBar) view.findViewById(R.id.rightSensor);
        rearProgressBar = (ProgressBar) view.findViewById(R.id.rearSensor);

        refresh.post(new Runnable() {
            @Override
            public void run() {
                leftProgressBar.setProgress(left);
            }
        });

        heartbeat = (TextView) view.findViewById(R.id.textView2);
        heartbeat.setText("Heartbeat not received..");
        leftDebug.setText("");
        middleDebug.setText("");
        rightDebug.setText("");
        rearDebug.setText("");

        return view;
    }

}
